	package utng.dom;

import java.util.List;
import java.util.ArrayList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import utng.model.Customer;

public class CustomerDOM {
	private String pathFile = "C:\\Users\\usuario\\eclipse-workspace\\MACCUnidadII\\src\\data\\customers.xml";

	public void add(Customer data) {
		try {
			Document document = DOMHelper.getDocument(pathFile);
			Element customers = document.getDocumentElement();
			// Createtag
			Element customer = document.createElement("customer");
			// Create id tag
			Element id = document.createElement("id");
			id.appendChild(document.createTextNode(data.getCustomerID()));
			customer.appendChild(id);
			// Create companyname tag
			Element name = document.createElement("companyname");
			name.appendChild(document.createTextNode(data.getCompanyName()));
			customer.appendChild(name);
			// Create name tag
			Element contact = document.createElement("contactname");
			contact.appendChild(document.createTextNode(data.getContactName()));
			customer.appendChild(contact);
			// Create name tag
			Element city = document.createElement("city");
			city.appendChild(document.createTextNode(data.getCity()));
			customer.appendChild(city);
			customers.appendChild(customer);
			// Write to file
			DOMHelper.saveXMLContent(document, pathFile);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void delete(String id) {
		try {
			Document document = DOMHelper.getDocument(pathFile);
			NodeList nodeList = document.getElementsByTagName("customer");
			for (int i = 0; i < nodeList.getLength(); i++) {
				Element customer = (Element) nodeList.item(i);
				if (customer.getElementsByTagName("id").item(0).getTextContent().equals(id)) {
					customer.getParentNode().removeChild(customer);
				}
			}
			DOMHelper.saveXMLContent(document, pathFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void update(Customer data) {
		try {
			Document document = DOMHelper.getDocument(pathFile);
			NodeList nodeList = document.getElementsByTagName("customer");
			for (int i = 0; i < nodeList.getLength(); i++) {
				Element customer = (Element) nodeList.item(i);
				if (customer.getElementsByTagName("id").item(0).getTextContent().equals(data.getCustomerID())) {
					customer.getElementsByTagName("companyname").item(0).setTextContent(data.getCompanyName());
					customer.getElementsByTagName("contactname").item(0).setTextContent(data.getContactName());
					customer.getElementsByTagName("city").item(0).setTextContent(data.getCity());
				}
			}
			DOMHelper.saveXMLContent(document, pathFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Customer findById(String id) {
		Customer customer = null;
		try {
			Document document = DOMHelper.getDocument(pathFile);
			NodeList nodeList = document.getElementsByTagName("customer");
			for (int i = 0; i < nodeList.getLength(); i++) {
				Element s = (Element) nodeList.item(i);
				if (s.getElementsByTagName("id").item(0).getTextContent().equals(id)) {
					customer = new Customer();
					customer.setCustomerID(id);
					customer.setCompanyName(s.getElementsByTagName("companyname").item(0).getTextContent());
					customer.setContactName(s.getElementsByTagName("contactname").item(0).getTextContent());
					customer.setCity(s.getElementsByTagName("city").item(0).getTextContent());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return customer;
	}

	public List<Customer> getCustomers() {
		List<Customer> customers = new ArrayList<Customer>();
		Document document = DOMHelper.getDocument(pathFile);
		NodeList nodeList = document.getElementsByTagName("customer");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element s = (Element) nodeList.item(i);
			Customer customer = new Customer();
			customer.setCustomerID(s.getElementsByTagName("id").item(0).getTextContent());
			customer.setCompanyName(s.getElementsByTagName("companyname").item(0).getTextContent());
			customer.setContactName(s.getElementsByTagName("contactname").item(0).getTextContent());
			customer.setCity(s.getElementsByTagName("city").item(0).getTextContent());
			customers.add(customer);
		}
		return customers;
	}
}
