package utng.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utng.dom.CustomerDOM;
import utng.model.Customer;

/**
 * Servlet implementation class CustomerController
 */
@WebServlet("/CustomerController")
public class CustomerController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Customer customer;
	private List<Customer> customers;
	private CustomerDOM customerDOM;
	private List<String> ids = new ArrayList<String>();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CustomerController() {
		super();
		customer = new Customer();
		customers = new java.util.ArrayList<>();
		customerDOM = new CustomerDOM();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("btn_save") != null) {
			customer.setCompanyName(request.getParameter("companyname"));
			customer.setContactName(request.getParameter("contactname"));
			customer.setCity(request.getParameter("city"));
		
			if (customer.getCustomerID() == "") {
				System.out.println("save");
				String newId =  "std"+String.format("%05d", 1);
				customer.setCustomerID(newId);
				System.out.println("save");
				if (customers.size()>0) {
					ids.clear();
					for(Customer s: customers) {
						ids.add(s.getCustomerID());
					}
					for (int i=0; i<=ids.size(); i++) {
						newId = "std"+String.format("%05d", i+1);
						if (!ids.contains(newId)) {
							customer.setCustomerID(newId);
							break;
						}
					}
				}
				customerDOM.add(customer);
			} else {
				customerDOM.update(customer);
			}
			customers = customerDOM.getCustomers();
			request.setAttribute("customers", customers);
			request.getRequestDispatcher("customer_list.jsp").forward(request, response);
		}else if(request.getParameter("btn_new")!=null) {
			customer = new Customer();
			request.getRequestDispatcher("customer_form.jsp").forward(request, response);
		}else if(request.getParameter("btn_edit")!=null) {
			try {
				String id = request.getParameter("id");
				customer = customerDOM.findById(id);	
			}catch (IndexOutOfBoundsException e) {
				customer = new Customer();
			}
		 request.setAttribute("customer", customer);
		 request.getRequestDispatcher("customer_form.jsp").forward(request, response);
		}else if(request.getParameter("btn_delete")!=null) {
			try {
				String id= request.getParameter("id");
				customerDOM.delete(id);
				customers = customerDOM.getCustomers();
			}catch (Exception e) {
				e.printStackTrace();
			}
			request.setAttribute("customers", customers);
			request.getRequestDispatcher("customer_list.jsp").forward(request, response);
		}else {
			customers = customerDOM.getCustomers();
			request.setAttribute("customers", customers);
			request.getRequestDispatcher("customer_list.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
