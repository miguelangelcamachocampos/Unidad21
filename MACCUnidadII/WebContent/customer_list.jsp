<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Custumer list</title>
</head>
<body>
<table border="1">
		<tr>
			<th>
			  <form action="CustomerController">
			  		<input type="submit" name="btn_new"
			  		value="New "/>
			  </form>
			</th>
			<td>Id</td>
			<td>ContactName</td>
			<td>CompanyName</td>
			<td>City</td>
		</tr>
		 <c:forEach var="customer" items="${customers}">
		 	 <tr>
		 	 	<td>
		 	 		<form action="CustomerController">
		 	 			<input type="hidden" name="id" value="${customer.customerID}">
		 	 			<input type="submit" name="btn_edit" value="Edit"/>
		 	 			 <input type="submit" name="btn_delete" value="Delete"/>
		 	 		</form>
		 	 	</td>
		 	 	<td>${customer.customerID }</td>
		 	 	<td>${customer.companyName }</td>
		 	 	<td>${customer.contactName }</td>
		 	 	<td>${customer.city }</td>
		 	 </tr>	
		 </c:forEach>
	</table>
</body>
</html>